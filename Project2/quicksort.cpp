#include <iostream>

template <class T>
int partition(T* arr, int start, int end) {
	T pivot = arr[start];

	int left = start + 1;
	int right = end;

	while (1) {
		while (left <= right && arr[left] <= pivot)
			left++;
		while (right >= left && arr[right] >= pivot)
			right--;
		if (right < left)
			break;
		// swap numbers where the left number is greater than pivot, and the right number is less than the pivot
		T temp = arr[left];
		arr[left] = arr[right];
		arr[right] = temp;
	}
	// place pivot so that numbers to the left are lower than it, and numbers to the right are greater than it
	arr[start] = arr[right];
	arr[right] = pivot;
	return right;
}

// Template allows for multiple data types
template <class T>
void quicksort(T* arr, int start, int end) {
	if (start >= end)
		return;
	int pivot = partition(arr, start, end);
	quicksort(arr, start, pivot - 1);
	quicksort(arr, pivot + 1, end);
}

int main() {
	int a[10] = { 4,6,2,7,2,8,5,2,4,7 };
	double b[10] = { 3.4, 3.1, 3.6, 3.1, 3.9, 3.8, 3.0, 3.3, 3.6, 3.5 };

	std::cout << "Sorting.." << std::endl;
	// Must pass in size of array here due to array decaying into pointer
	quicksort(a, 0, (sizeof(a) / sizeof(a[0])) - 1);
	quicksort(b, 0, (sizeof(b) / sizeof(b[0])) - 1);

	std::cout << "Sorted" << std::endl;
	
	for (int i = 0; i < 10; i++)
		std::cout << a[i] << std::endl;
	std::cout << std::endl;
	for (int i = 0; i < 10; i++)
		std::cout << b[i] << std::endl;

	while (1);
	return 0;
}